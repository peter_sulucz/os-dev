LD=ld
CC=gcc
NASM=nasm
QEMU=qemu

SOURCEDIR = src
TEMPDIR = temp
BOOTDIR = boot

all: floppy.img

.SUFFIXES: .o .rs .asm

.PHONY: clean dependencies run

.c.o:
	$(CC) -c -m32 -std=c99 -o $@ main.c -ffreestanding -nostdlib

.asm.o:
	$(NASM) -o $@ $<

floppy.img: $(TEMPDIR)/bootloader.bin $(TEMPDIR)/main.bin
	dd if=/dev/zero of=$@ bs=512 count=8 &>/dev/null
	cat $^ | dd if=/dev/stdin of=$@ conv=notrunc &>/dev/null

$(TEMPDIR)/*.o:
	mkdir $(TEMPDIR)
	make -C $(SOURCEDIR)
	

$(TEMPDIR)/bootloader.bin: $(TEMPDIR)/*.o
	make -C $(BOOTDIR)

$(TEMPDIR)/main.bin: linker.ld $(TEMPDIR)/main.o $(TEMPDIR)/*.o
	$(LD) -m elf_i386 -o $@ -T $^ 

run: floppy.img
	$(QEMU) -fda $<

clean:
	rm -f *.bin *.o *.img *~
	rm -rf $(TEMPDIR)
