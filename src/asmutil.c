#include "asmutil.h"
//read a byte from a port
//thanks to http://www.osdever.net/bkerndev/Docs/creatingmain.htm
unsigned char readByte(unsigned short port)
{
	unsigned char val;
	__asm__ __volatile__ ("inb %1, %0" : "=a" (val) : "dN" (port));
	return val;
}
//push a byte to a port
//thanks to http://www.osdever.net/bkerndev/Docs/creatingmain.htm
void pushbyte(unsigned short port, unsigned char data)
{
	__asm__ __volatile__ ("outb %1, %0" : : "dN" (port), "a" (data));
}
