#include "keyboard.h"
#include "kernalio.h"
#include "Time.h"

void main(){
	clearScreen();
	writeLogo();
	writeTest(INIT_keyboard(), "Keyboard");
	datetime time = {0,0,0,0,0,0,0};
	datetime blank = {0,0,0,0,0,0,0};
	INIT_dateTime(&time);
	writeTest(!EQUAL_dateTime(&time, &blank), "System Time");
	write("Date: ");
	putui(time.month);
	write("/");
	putui(time.day);
	write("/");
	putui(time.year);
	write(" Time: ");
	putui(time.hour);
	write(":");
	putui(time.minute);
	write(":");
	putui(time.second);
	write("\n");
	for(int i = 0; i < 10; i++)
		writeTest(i % 2, "Test");
	while(1)
	{
		char key;
		if((key = poll_keyboard()) > 32)
		{
			putc(key);
		}
	}
	return;
}
