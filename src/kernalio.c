#include "kernalio.h"
static short OUTPUTX = 0;
static short OUTPUTY = 0;
static short FOREGROUND = 0xf;
static short BACKGROUND = 0;

short makeVGAENTRY(char c, short fg, short bg)
{
	short color = fg | (bg << 4);
	return (color << 8) | ((short)c);
}
void shiftVGABufferUP()
{
	short* ptr = (short *)0xb8000;
	for(int r = 0; r < 25; r++)
	{
		for(int c = 0; c < 80; c++)
		{
			*((short*)0xb8000 + c + r*80) = r == 24 ? 0 : *((short*)0xb8000 + c + (r+1)*80);
		}
	}
}
void clearScreen()
{
	for(int i = 0; i < 80*25; i++)
	{
		putchar(i%80, i/80, 0, 0, BACKGROUND);
	}
}
void writeLogo()
{
	write("______           _  __ _                 \n");
	write("| ___ \\         | |/ _| |                \n");
	write("| |_/ / __ _  __| | |_| |_   _  ___ _ __ \n");
	write("| ___ \\/ _` |/ _` |  _| | | | |/ _ \\ '__|\n");
	write("| |_/ / (_| | (_| | | | | |_| |  __\\/ |   \n");
	write("\\____/ \\__,_|\\__,_|_| |_|\\__, |\\___|_|   \n");
	write("                          __/ |          \n");
	write("                         |___/           \n");
}
void putchar(short x, short y, char c, short fg, short bg)
{
	*((short*)0xb8000 + x + y*80) = makeVGAENTRY(c, fg, bg);
}
void writeColor(char * str, short fg, short bg)
{
	char *p = str;
	while(*p != 0)
	{	if(OUTPUTX == 80)
		{
			OUTPUTX=0;
			OUTPUTY++;
		}
		if(OUTPUTY == 25)
		{
			shiftVGABufferUP();
			OUTPUTY = 24;
			OUTPUTX = 0;
		}
		if(*p == '\n')
		{
			OUTPUTY++;
			OUTPUTX = 0;
		}
		else if(*p == '\r')
		{
			OUTPUTX = 0;
		}
		else
		{
			putchar(OUTPUTX++, OUTPUTY, *p, fg, bg);
		}
		p++;
	}
}
void putc(char c)
{
	if(OUTPUTX == 80)
	{
		OUTPUTX=0;
		OUTPUTY++;
	}
	if(OUTPUTY == 25)
	{
		shiftVGABufferUP();
		OUTPUTY = 24;
		OUTPUTX = 0;
	}
	if(c == '\n')
	{
		OUTPUTY++;
		OUTPUTX = 0;
	}
	else if(c == '\r')
	{
		OUTPUTX = 0;
	}
	else
	{
		putchar(OUTPUTX++, OUTPUTY, c, FOREGROUND, BACKGROUND);
	}
}
void write(char * str)
{
	writeColor(str, FOREGROUND, BACKGROUND);
}

void writeTest(bool pass, char * name)
{
	write("[");
	pass ? writeColor("PASS", 2, BACKGROUND) : writeColor("FAIL", 0xc, BACKGROUND);
	write("]   ");
	write(name);
	write("\n");
}
void putui(int num)
{
	if(num < 10)
		putc((char)num + 48);
	else
	{
		putui(num / 10);
		putc(num % 10+48);
	}	
}
