#ifndef TIME_H
#define TIME_H
#include "types.h"
#include "asmutil.h"
typedef struct {
	unsigned int century;
	unsigned char second;
	unsigned char minute;
	unsigned char hour;
	unsigned char day;
	unsigned char month;
	unsigned int year;
} datetime;

void INIT_dateTime(datetime* date);
bool EQUAL_dateTime(datetime* a, datetime* b);
#endif