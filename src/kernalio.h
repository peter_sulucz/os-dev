#ifndef KERNALIO_H
#define KERNALIO
#include "types.h"
void clearScreen();
void writeLogo();
void putchar(short x, short y, char c, short fg, short bg);
void write(char * str);
void writeColor(char * str, short fg, short bg);
void writeTest(bool pass, char * name);
void putc(char c);
void putui(int num);
#endif
