#include "Time.h"
enum 
{
	cmos_address = 0x70,
	cmos_data = 0x71
};
int getUpdateInProgressFlag()
{
	pushbyte(cmos_address, 0x0A);
	return (readByte(cmos_data) & 0x80);
}
unsigned char getRTCRegister(int reg)
{
	pushbyte(cmos_address, reg);
	return readByte(cmos_data);
}
void INIT_dateTime(datetime* date)
{
	// place holders to ensure consistancy
	datetime previous;
	unsigned char registerB;

	// run until the update is complete
	while(getUpdateInProgressFlag());
	date->second = getRTCRegister(0x00);
	date->minute = getRTCRegister(0x02);
	date->hour = getRTCRegister(0x04);
	date->day = getRTCRegister(0x07);
	date->month = getRTCRegister(0x08);
	date->year = getRTCRegister(0x09);

	do{
		previous.second = date->second;
		previous.minute = date->minute;
		previous.hour = date->hour;
		previous.day = date->day;
		previous.month = date->month;
		previous.year = date->year;

		while(getUpdateInProgressFlag());
		date->second = getRTCRegister(0x00);
		date->minute = getRTCRegister(0x02);
		date->hour = getRTCRegister(0x04);
		date->day = getRTCRegister(0x07);
		date->month = getRTCRegister(0x08);
		date->year = getRTCRegister(0x09);

	} while((previous.second != date->second) || (previous.minute != date->minute));

	registerB = getRTCRegister(0x0B);

	if(!(registerB & 0x04))
	{
		date->second = (date->second & 0x0F) + ((date->second / 16) * 10);
		date->minute = (date->minute & 0x0F) + ((date->minute / 16) * 10);
		date->hour = ((date->hour & 0x0F) + ((date->hour & 0x70) / 16) * 10) | (date->hour & 0x80);
		date->day = (date->day & 0x0F) + ((date->day / 16) * 10);
		date->month = (date->month & 0x0F) + ((date->month / 16) * 10);
		date->year = (date->year & 0x0F) + ((date->year / 16) * 10);
	}
}
bool EQUAL_dateTime(datetime* a, datetime* b)
{
	char* one = (char*)a;
	char* two = (char*)b;
	while(one < a+sizeof(datetime))
	{
		if(one++ != two++)
			return false;
	}
}