#include "keyboard.h"
#include "asmutil.h"
static char keyMap[] = "~~1234567890-=~~qwertyuiop[]~~asdfghjkl;\'`~\\zxcvbnm,./~~~ ~~~~~~~~~~~";
//privates
unsigned char keyboardRead()
{
	return readByte(KEYBOARD_BUFFER);
}
bool keyboardINPUTBufferFull()
{
	return readByte(KEYBOARD_OUTPUT) & 0x02;
}
bool keyboardOUTPUTBufferFull()
{
	return readByte(KEYBOARD_OUTPUT) & 0x01;
}
void writeKeyboardBuffer(unsigned char buffer, unsigned char value)
{
	pushbyte(buffer, value);
}
void keyboardUpdate(char code)
{
	while(!keyboardINPUTBufferFull());
	writeKeyboardBuffer(KEYBOARD_BUFFER, 0xf4);
}

//initialize the keyboard
bool INIT_keyboard()
{
	writeKeyboardBuffer(KEYBOARD_OUTPUT, KEYBOARD_ACTIVATE);
	for(int i = 0; i<0xffff; i++)
		if(!keyboardINPUTBufferFull())
			break;
	writeKeyboardBuffer(KEYBOARD_BUFFER, 0xf4);
	for(int i = 0; i < 100; i++)
	{
		for(int j = 0; j < 0xffff; j++)
			if(!keyboardINPUTBufferFull())
				break;
		if(keyboardRead() == 0xf4)
		{
			return true;
		}
	}
	return false;
}

char poll_keyboard()
{
	while(!keyboardOUTPUTBufferFull());
	char val = (char)keyboardRead();	
	if(val < 60)
		val = keyMap[val];
	else
		val = 126;
	//keyboardUpdate(val);
	return val;
}
